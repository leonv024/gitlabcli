import requests, json

def edit_repo(api_url, user, token, item, value, id):
    headers = {"Private-Token": "%s" % token}
    payload = {item: value}
    login = requests.put(api_url + 'projects/' + id, headers=headers, data=payload)

    if login.status_code == 200:
        print('\033[32m[OK]\033[0m Project edited')
    else:
        print('\033[31m[%s]\033[0m Failed to edit project' % login.status_code)
